import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Mr. Nice', isLive: true },
    { id: 12, name: 'Narco', isLive: false },
    { id: 13, name: 'Bombasto', isLive: true },
    { id: 14, name: 'Celeritas', isLive: false },
    { id: 15, name: 'Magneta', isLive: false },
    { id: 16, name: 'RubberMan', isLive: true },
    { id: 17, name: 'Dynama', isLive: true },
    { id: 18, name: 'Dr IQ', isLive: true },
    { id: 19, name: 'Magma', isLive: false },
    { id: 20, name: 'Tornado', isLive: false }
];
