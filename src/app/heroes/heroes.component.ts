import { HeroService } from './../hero.service';
import { Component, OnInit } from '@angular/core';

import { Hero } from './../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  selectedHero: Hero;
  heroes: Hero[];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.setHeroes();
  }

  onHeroSelect(hero: Hero) {
    this.selectedHero = hero;
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }

    this.heroService.post({ name } as Hero)
      .subscribe(
        (hero: Hero) => this.heroes.push(hero)
      );
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero)
    this.heroService.delete(hero)
      .subscribe();
  }

  private setHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
  }
}
