import { HeroService } from './../hero.service';
import { Hero } from './../hero';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  hero: Hero;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private heroService: HeroService
  ) { }

  ngOnInit() {
    this.setHero();
  }

  goBack() {
    this.location.back();
  }

  save(hero: Hero) {
    this.heroService.update(hero)
      .subscribe(
        _ => this.goBack()
      );
  }

  private setHero() {
    const heroId = +this.route.snapshot.paramMap.get('id');

    this.heroService.getHero(heroId)
      .subscribe(hero =>
        this.hero = hero
      );
  }

}
