export class Hero {
    id: number;
    name: string;
    isLive: boolean;
}